#!/bin/sh
# Delete every Docker containers
# Must be run first because images are attached to containers
podman rm -f $(podman ps -a -q)